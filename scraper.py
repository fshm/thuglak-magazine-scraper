from urllib.parse import urlencode
from bs4 import BeautifulSoup
from PIL import Image
from pickle import load, dump
from os import makedirs
from os.path import exists
import os
from pypandoc import convert_text


class ThuglakScraper:

    def __init__(self, session: 'Login credentials session object'):
        self.session: 'Login credentials session object' = session

    def file_structure(self, links: list[str]) -> None:
        link: list[str] = links
        # parent warp folder
        parent_folder_name: str = 'datasets/'
        if exists(parent_folder_name):
            pass
        else:
            makedirs(parent_folder_name)
        # sub folders
        for folder_name in link:
            folder: str = parent_folder_name + folder_name[-10:]
            if exists(folder):
                print(f'File exists: {folder}')
            else:
                makedirs(folder)

    def generate_link(self) -> list[str]:
        base_url: str = 'https://thuglak.com/thuglak/archive.php?'
        link_list: list[str] = []
        archive_title_list: list[str] = []
        for x in range(
                0, 640 + 20,
                20):  # Generating link for all the pages - page 0 to 640
            query: dict[str, int] = {'argCnt': x}
            start_link: str = base_url + urlencode(query)
            _dump: 'get response object' = self.session.get(start_link)
            BsObj: 'soup object' = BeautifulSoup(_dump.text, 'lxml')
            l = BsObj.find('td').find_all('a')
            for y in l[0:-6]:
                archive_title_list.append(y.text.strip())
                if y['href'] != 'index1.php?cPath=1':
                    link_list.append('https://thuglak.com/thuglak/' +
                                     y['href'])
        del query
        for x in range(0, 640 + 20, 20):
            query: dict[str, int] = {'argCnt': x}
            start_link: str = base_url + urlencode(query)
            try:
                link_list.remove(start_link)  # delete unwanted gen links
            except ValueError as e:
                print(start_link + ' Not Present')
        del query
        link_list: list[str] = list(set(link_list))  #each link are Unique
        with open("articles_link", "wb") as fp:
            dump(link_list, fp)  #Save links in a pickle file
        print("GenLink Completed!!!")
        return link_list

    def artical_link(self, path: str, url: str) -> dict[str, str]:
        page_arc_list: dict[str, str] = {}
        perArtical: 'get response object' = self.session.get(url)
        BsObj: 'soup object' = BeautifulSoup(perArtical.text, 'lxml')
        try:
            img: 'Cover photo' = BsObj.find('div', class_='attai').find('img')
            img_url: str = 'https://thuglak.com/thuglak/' + img['src']
            if exists(f'{path}Cover-{img_url[-8:]}.jpg'):
                pass
            else:
                img = Image.open(self.session.get(img_url, stream=True).raw)
                print(f'{path}Cover-{img_url[-8:]}.jpg')
                img.save(f'{path}Cover-{img_url[-8:]}.jpg')
        except:
            print(f'{url[-10:]} Cover photo Not Found!!!')
        l: list[any] = BsObj.find("ul", class_="rhsWeeklylist").find_all('a')
        for y in l:
            key: str = y.text.strip()
            print(key)
            value: str = 'https://thuglak.com/thuglak/' + y['href']
            page_arc_list[key] = value
        # print(page_arc_list)
        return page_arc_list

    def html_to_docx(self, path: str, filename: str, url: str):
        if len(filename) > 20:
            filename = filename[:20]
        if '/' in filename:
            filename = filename.replace('/','')
        if exists(f'{path}{filename}.docx'):
            pass
        else:
            article: 'get response' = self.session.get(url)
            if article.status_code == 200:
                BsObj: 'soup object' = BeautifulSoup(article.text, 'lxml')
                article_div: 'div to be doc' = BsObj.find('div', class_='innerLhs')
                os.environ.setdefault('PYPANDOC_PANDOC', '/usr/bin/pandoc')
                source = convert_text(source=article_div, to='md', format='html')
                export = convert_text(source=source,
                                    to='docx',
                                    format='md',
                                    outputfile=f"{path}{filename}.docx")
                print("done")
            else:
                print(
                    f'URL: {url} \n Statuscode: {article.status_code} \n Check link'
                )


# generateLink(session)
# test: dict = articalLink(
# session,
# url="https://thuglak.com/thuglak/archivefinal.php?sub1=01_09_2021")
# x = test.keys()
# x = test[list(x)[0]]
# artlink = session.get(x)
