from requests import Session
from datetime import date
from .scraper import ThuglakScraper
from os.path import exists, getmtime
from pickle import load


def login_session(
        username: str ,
        password: str ) -> 'Logged in Session Object':
    # Login
    login_url = "https://thuglak.com/thuglak/login.php?action=progress"
    data: dict = {'email_address': username, 'password': password}
    session = Session()
    session.post(url=login_url, data=data)
    return session


def main() -> None:
    # Plz register ur login credentials
    session: 'logged in session' = login_session(username = 'xxx@yyy.zzz', password = 'xxxyyzzz')
    ScraperObj = ThuglakScraper(session)
    if exists('articles_link'):
        if date.fromtimestamp(getmtime('articles_link')) == date.today():
            pass
        else:
            ScraperObj.generate_link()
    else:
        ScraperObj.generate_link()
    # Folder organization
    if exists('articles_link'):
        _file: 'pickle read object' = open('articles_link', 'rb')
        magazine_links: list[str] = load(_file)
        ScraperObj.file_structure(links=magazine_links)
    else:
        magazine_links: list[str] = ScraperObj.generate_link()
        ScraperObj.file_structure(links=magazine_links)
    for url in magazine_links:  # for each magazine link
        parent_folder_name: str = 'datasets/'
        path: str = parent_folder_name + url[-10:] + '/'
        articles_link: dict[str, str] = ScraperObj.artical_link(path=path,
                                                                url=url)
        for filename,docxlink in articles_link.items():
            print(filename, docxlink)
            ScraperObj.html_to_docx(path=path, filename=filename, url=docxlink)
