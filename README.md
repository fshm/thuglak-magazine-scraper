# Thuglak Magazine Scraper

### Dependence
Install all dependence package `pip install -r thuglak-magazine-scraper/requirements.txt`

### Run The Program
    python -m thuglak-magazine-scraper

### License
This project is license under **AGPL v3**. Read `LICENSE` file for more infomration.
